% %Sample from the Gaussian process distribution
% nb_of_samples = 41  % Number of points in each function
% number_of_functions = 5  % Number of functions to sample
% %Independent variable samples
% X = np.expand_dims(np.linspace(-4, 4, nb_of_samples), 1)
% sigma = exponentiated_quadratic(X, X)  % Kernel of data points
% 
% % Draw samples from the prior at our data points.
% % Assume a mean of 0 for simplicity
% ys = np.random.multivariate_normal(
%     mean=np.zeros(nb_of_samples), cov=Σ, 
%     size=number_of_functions)

close all
clear

n_samples = 4100  % Number of points in each function
n_functions = 5  % Number of functions to sample
X = linspace(-4, 4, n_samples);

D = squareform(pdist(X', 'squaredeuclidean'));

sigma = .5*exp(-1 * D/2);
hold on
plot(mvnrnd(zeros(1,n_samples)',sigma,n_functions)')